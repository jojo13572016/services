#/bin/bash
sudo docker run \
  -P --name rs1_srv1 \
  -d jojo13572001/mongodb \
  --replSet rs1 \
  --noprealloc --smallfiles

sudo docker run \
  -P --name rs1_srv2 \
  -d jojo13572001/mongodb \
  --replSet rs1 \
  --noprealloc --smallfiles

sudo docker run \
  -P --name rs1_srv3 \
  -d jojo13572001/mongodb \
  --replSet rs1 \
  --noprealloc --smallfiles
