#/bin/bash
sudo docker run \
  -P --name rs2_srv1 \
  -d jojo13572001/mongodb \
  --replSet rs2 \
  --noprealloc --smallfiles

sudo docker run \
  -P --name rs2_srv2 \
  -d jojo13572001/mongodb \
  --replSet rs2 \
  --noprealloc --smallfiles

sudo docker run \
  -P --name rs2_srv3 \
  -d jojo13572001/mongodb \
  --replSet rs2 \
  --noprealloc --smallfiles
